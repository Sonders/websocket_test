from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync

from .services import RoomService
from .serializers import PlayerSerializer


import json


class RoomConsumer(WebsocketConsumer):
    def connect(self):
        room_id = self.scope['url_route']['kwargs']['id']

        room_service = RoomService()
        room = room_service.get_room_by_id(room_id)

        if room.opponent:
            async_to_sync(self.channel_layer.group_send)(f'room-{room_id}', {
                'type': 'send_event',
                'data': json.dumps({
                    'type': 'EVENT_OPPONENT_FOUND',
                    'opponent': PlayerSerializer(room.opponent).data
                })
            })

        async_to_sync(self.channel_layer.group_add)(f'room-{room_id}', self.channel_name)

        self.accept()

    def disconnect(self, code):
        self.close(code)

    def send_event(self, event):
        self.send(event['data'])
