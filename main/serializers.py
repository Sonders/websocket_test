from rest_framework import serializers
from .models import Room, Player


class PlayerSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()

    class Meta:
        model = Player
        fields = '__all__'

    def get_username(self, obj):
        user = obj.person.person_users.first()

        return user.username


class RoomSerializer(serializers.ModelSerializer):
    creator = PlayerSerializer()
    opponent = PlayerSerializer()
    is_owner = serializers.SerializerMethodField()

    class Meta:
        model = Room
        fields = '__all__'

    def get_is_owner(self, obj):
        return obj.creator.person_id == self.context['user'].person_id
