from rest_framework.authtoken.models import Token
from rest_framework.exceptions import NotFound
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.hashers import SHA1PasswordHasher

from . import models


class Service:
    def __init__(self):
        pass


class UserService(Service):
    def __init__(self):
        super().__init__()

    def auth(self, username: str, password: str):
        password = SHA1PasswordHasher().encode(password, 'salt').split('$')[-1]

        try:
            user = models.User.objects.get(username=username, password=password)
        except ObjectDoesNotExist:
            raise NotFound

        token = Token.objects.get_or_create(user=user)

        return token


class RoomService(Service):
    def __init__(self):
        super().__init__()

    def get_room_by_id(self, room_id: int):
        try:
            room = models.Room.objects.get(id=room_id)
        except ObjectDoesNotExist:
            raise NotFound

        return room

    def find_or_create_room(self, person_id: int):
        free_rooms = models.Room.objects.filter(opponent=None).exclude(creator__person_id=person_id)

        player = models.Player.objects.get(person_id=person_id)

        if free_rooms.exists():
            room = free_rooms.first()
            room.opponent = player
            room.save()

            return room

        return models.Room.objects.create(creator=player)
