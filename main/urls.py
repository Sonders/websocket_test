from django.urls import path

from . import views


urlpatterns = [
    path('auth/', views.AuthAPIView.as_view()),
    path('rooms/find', views.RoomFinderAPIView.as_view()),
    path('rooms/<int:room_id>', views.RoomAPIView.as_view()),
]
