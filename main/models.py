from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField

from . import managers


class Person(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100, null=True)
    update_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'persons'


class User(AbstractUser):
    password = models.CharField(max_length=128, null=True)

    person = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, related_name='person_users')
    email = models.EmailField(max_length=255, null=True, unique=True)
    roles = ArrayField(base_field=models.CharField(max_length=255))
    creation_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    status = models.SmallIntegerField(default=0)

    objects = managers.UserManager()

    REQUIRED_FIELDS = []

    class Meta:
        db_table = 'users'


class Player(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    class Meta:
        db_table = 'players'


class Room(models.Model):
    creator = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='creator_rooms')
    opponent = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='opponent_rooms', null=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'rooms'
