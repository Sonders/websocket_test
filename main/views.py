from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from . import services
from . import serializers


class AuthAPIView(APIView):
    def post(self, request):
        username = request.data.get('username')
        password = request.data.get('password')

        user_service = services.UserService()
        token, _ = user_service.auth(username, password)

        return Response({'token': str(token)})


class RoomAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, room_id):
        room_service = services.RoomService()
        room = room_service.get_room_by_id(room_id)

        serializer = serializers.RoomSerializer(room, context={'user': request.user})

        return Response(serializer.data)


class RoomFinderAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        room_service = services.RoomService()
        room = room_service.find_or_create_room(request.user.person_id)

        serializer = serializers.RoomSerializer(room, context={'user': request.user})

        return Response(serializer.data)
