from django.urls import re_path

from main import consumers

url_patterns = [
    re_path('^ws/rooms/(?P<id>\w+)/$', consumers.RoomConsumer.as_asgi()),
]
