from django.urls import path
from . import views


urlpatterns = [
    path('', views.index),
    path('rooms/find', views.find_room),
    path('rooms/<int:room_id>', views.room),
]
