from django.shortcuts import render


def index(request):
    return render(request, 'login.html')


def find_room(request):
    return render(request, 'room-finder.html')


def room(request, room_id):
    return render(request, 'room.html', {'room_id': room_id})
