FROM python:3.8

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV POETRY_VIRTUALENVS_CREATE="false"

COPY . .
RUN pip install poetry
RUN poetry install --no-interaction --no-ansi --no-dev
